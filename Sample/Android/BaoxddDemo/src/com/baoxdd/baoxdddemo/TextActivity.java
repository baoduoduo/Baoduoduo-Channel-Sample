package com.baoxdd.baoxdddemo;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import org.apache.http.entity.StringEntity;

import com.baoxdd.baoxdddemo.model.Data;
import com.google.gson.Gson;
import com.lidroid.xutils.HttpUtils;
import com.lidroid.xutils.exception.HttpException;
import com.lidroid.xutils.http.RequestParams;
import com.lidroid.xutils.http.ResponseInfo;
import com.lidroid.xutils.http.callback.RequestCallBack;
import com.lidroid.xutils.http.client.HttpRequest.HttpMethod;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;

public class TextActivity extends Activity {

	private TextView text;
	private int whichRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_text);

		text = (TextView) findViewById(R.id.text);

		whichRequest = getIntent().getIntExtra("main", 0);
		switch (whichRequest) {
		case 3:
			// try {
			// doRequestUserOrder();
			// } catch (UnsupportedEncodingException e) {
			// // TODO Auto-generated catch block
			// e.printStackTrace();
			// }
			SimpleAsyncTask sat = new SimpleAsyncTask();
			sat.execute();
			break;

		default:
			break;
		}
	}

	private void doRequestUserOrder() throws UnsupportedEncodingException {
		HttpUtils http = new HttpUtils();
		RequestParams params = new RequestParams();
		// params.setBodyEntity(new
		// StringEntity("{\"data\":{\"FullTextSearch\":\"王维一\",\"InsuranceOrderStatu\":\"2\"},\"paging\":{\"pageNumber\":1,\"pageSize\":5},\"userCookie\":{\"channelNumber\":\"951999MB\",\"UserID\":\"0000000001\",\"m\":\"7b16f939c2ec119ec1a73784de0d69b3\"}}"));
		OrderRequest or = new OrderRequest();
		or.getData().setFullTextSearch("123");
		or.getData().setInsuranceOrderStatu("2");
		or.getPaging().setPageNumber(1);
		or.getPaging().setPageSize(5);
		or.getUserCookie().setChannelNumber("951999MB");
		or.getUserCookie().setUserID("0000000001");
		or.getUserCookie().setM("7b16f939c2ec119ec1a73784de0d69b3");
		Gson gson = new Gson();
		String requestStr = gson.toJson(or);
		params.setBodyEntity(new StringEntity(requestStr, "UTF-8"));

		http.send(HttpMethod.POST, Constant.API_URL
				+ "/v2/Channel/Order/UserOrders", params,
				new RequestCallBack<String>() {
					@Override
					public void onLoading(long total, long current,
							boolean isUploading) {
					}

					@Override
					public void onSuccess(ResponseInfo<String> responseInfo) {
						text.setText(responseInfo.result);
					}

					@Override
					public void onStart() {
					}

					@Override
					public void onFailure(HttpException error, String msg) {
					}
				});

	}


	private class SimpleAsyncTask extends AsyncTask<Void,Void, String> {

		@Override
		protected String doInBackground(Void... params) {
			String jsonStr = "{\"data\":{\"FullTextSearch\":\"王维一\",\"InsuranceOrderStatu\":\"2\"},\"paging\":{\"pageNumber\":1,\"pageSize\":5},\"userCookie\":{\"channelNumber\":\"951999MB\",\"UserID\":\"0000000001\",\"m\":\"7b16f939c2ec119ec1a73784de0d69b3\"}}";

			String returnLine = "";
			try {
				URL my_url = new URL(Constant.API_URL
						+ "/v2/Channel/Order/UserOrders");
				HttpURLConnection connection = (HttpURLConnection) my_url
						.openConnection();
				connection.setDoOutput(true);

				connection.setDoInput(true);

				connection.setRequestMethod("POST");

				connection.setUseCaches(false);

				connection.setInstanceFollowRedirects(true);

				connection.setRequestProperty("Content-Type", "application/json");

				connection.connect();
				DataOutputStream out = new DataOutputStream(
						connection.getOutputStream());

				byte[] content = jsonStr.getBytes("utf-8");

				out.write(content, 0, content.length);
				out.flush();
				out.close(); // flush and close

				BufferedReader reader = new BufferedReader(new InputStreamReader(
						connection.getInputStream(), "utf-8"));

				// StringBuilder builder = new StringBuilder();

				String line = "";

				System.out.println("Contents of post request start");

				while ((line = reader.readLine()) != null) {
					// line = new String(line.getBytes(), "utf-8");
					returnLine += line;

					System.out.println(line);

				}

				System.out.println("Contents of post request ends");

				reader.close();
				connection.disconnect();
				System.out.println("========返回的结果的为========" + returnLine);

			} catch (Exception e) {
				e.printStackTrace();
			}

			return returnLine;
		}

		@Override
		protected void onPostExecute(String result) {
			text.setText(result);
			
		}

		@Override
		protected void onPreExecute() {
		}

	}
}
