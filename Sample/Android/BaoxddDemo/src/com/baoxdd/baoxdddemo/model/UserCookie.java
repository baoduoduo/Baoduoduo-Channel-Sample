package com.baoxdd.baoxdddemo.model;
public class UserCookie{
		private String channelNumber;
		private String UserID;
		private String m;
		
		public String getChannelNumber() {
			return channelNumber;
		}
		public void setChannelNumber(String channelNumber) {
			this.channelNumber = channelNumber;
		}
		public String getUserID() {
			return UserID;
		}
		public void setUserID(String userID) {
			UserID = userID;
		}
		public String getM() {
			return m;
		}
		public void setM(String m) {
			this.m = m;
		}
	}
