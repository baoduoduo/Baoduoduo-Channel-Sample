package com.baoxdd.baoxdddemo;

import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public class WebActivity extends Activity {
	
	private WebView web;
	private int whichRequest;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_web);
		
		web = (WebView) findViewById(R.id.web);
//		web.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		web.getSettings().setUseWideViewPort(true);
		web.getSettings().setLoadWithOverviewMode(true);
		
		whichRequest = getIntent().getIntExtra("main", 0);
		web.setWebViewClient(new WebViewClient(){
             @Override
             public boolean shouldOverrideUrlLoading(WebView view, String url){
                      
                     return false;
                      
             }
		});
		
		switch (whichRequest) {
		case 0:
			web.loadUrl(Constant.HOME_URL);
			break;
		case 1:
			web.loadUrl(Constant.QUOTE_URL);
			break;
		case 2:
			web.loadUrl(Constant.ORDER_DETAIL_URL);
			break;

		default:
			break;
		}
	}
	
	
	

}
