package com.baoxdd.baoxdddemo;

import com.baoxdd.baoxdddemo.model.Data;
import com.baoxdd.baoxdddemo.model.Paging;
import com.baoxdd.baoxdddemo.model.UserCookie;

public class OrderRequest {
	
	private Data data;
	private Paging paging;
	private UserCookie userCookie;
	
	public Data getData() {
		return data;
	}

	public void setData(Data data) {
		this.data = data;
	}

	public Paging getPaging() {
		return paging;
	}

	public void setPaging(Paging paging) {
		this.paging = paging;
	}

	public UserCookie getUserCookie() {
		return userCookie;
	}

	public void setUserCookie(UserCookie userCookie) {
		this.userCookie = userCookie;
	}

	public OrderRequest() {
		super();
		this.data = new Data();
		this.paging = new Paging();
		this.userCookie = new UserCookie();
	}

	
	
	
	
	
}
