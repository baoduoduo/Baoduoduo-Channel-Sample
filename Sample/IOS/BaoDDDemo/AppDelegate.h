//
//  AppDelegate.h
//  BaoDDDemo
//
//  Created by 柳恒博 on 16/1/14.
//  Copyright © 2016年 柳恒博. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

