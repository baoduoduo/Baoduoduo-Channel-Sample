//
//  WebViewController.m
//  BaoDDDemo
//
//  Created by 柳恒博 on 16/1/14.
//  Copyright © 2016年 柳恒博. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()

@property (nonatomic, strong) UIWebView *webView;

@end

@implementation WebViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createView];
}

- (void)createView {
    self.webView = [[UIWebView alloc] initWithFrame:[UIScreen mainScreen].bounds];
    [self.webView setScalesPageToFit:YES];           //自动缩放以适应屏幕
    [self.view addSubview:self.webView];
    NSURL *url = [NSURL URLWithString:self.path];
    [self.webView loadRequest:[NSURLRequest requestWithURL:url]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
