//
//  ViewController.m
//  BaoDDDemo
//
//  Created by 柳恒博 on 16/1/14.
//  Copyright © 2016年 柳恒博. All rights reserved.
//

#import "ViewController.h"
#import "AFNetworking.h"
#import "WebViewController.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, strong) NSArray *array;
@property (nonatomic, assign) BOOL isAppear;
@property (nonatomic, strong) UITextView *textView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.array = @[@"首页", @"车辆报价", @"订单详情", @"订单列表"];
    [self configureNavigationBar];
    [self createTextView];
}

- (void)viewWillAppear:(BOOL)animated {
    UITableView *tableView = (UITableView *)[self.view viewWithTag:101];
    [tableView removeFromSuperview];
    _isAppear = NO;
}

//配置导航条
- (void)configureNavigationBar {
    self.navigationItem.title = @"保多多";
    UIBarButtonItem *leftItem = [[UIBarButtonItem alloc] initWithTitle:@"保多多功能" style:UIBarButtonItemStylePlain target:self action:@selector(handleTableView:)];
    self.navigationItem.leftBarButtonItem = leftItem;
}

- (void)createTextView {
    self.textView = [[UITextView alloc] initWithFrame:CGRectMake(20, 80, 335, 527)];
    self.automaticallyAdjustsScrollViewInsets = NO;
    //设置UITextView的边框线
    _textView.layer.borderWidth = 2;
    //设置边框的颜色
    _textView.layer.borderColor = [[UIColor blackColor] CGColor];
    [self.view addSubview:_textView];
}

#pragma mark - tableView DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _array.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor lightGrayColor];
    cell.textLabel.text = _array[indexPath.row];
    return cell;
}

#pragma mark - tableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.row) {
        case 0:
            [self homePage];
            break;
        case 1:
            [self carQuote];
            break;
        case 2:
            [self orderDetail];
            break;
        case 3:
            [self orderList];
            break;
            
        default:
            break;
    }
}

#pragma mark - handleAction
- (void)handleTableView:(UIBarButtonItem *)sender {
    if (!_isAppear) {
        UITableView *leftView = [[UITableView alloc] initWithFrame:CGRectMake(0, 64, 100, 200)];
        leftView.delegate = self;
        leftView.dataSource = self;
        leftView.tag = 101;
        [leftView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cell"];
        [self.view addSubview:leftView];
    }else {
        UITableView *tableView = (UITableView *)[self.view viewWithTag:101];
        [tableView removeFromSuperview];
    }
    _isAppear = !_isAppear;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - 宝多多功能

//首页 -- HTML页面
- (void)homePage {
    NSString *url = @"http://m.baoxianduoduo.com/QybApp/Channel/IndexFromChannel?c=951999MB&t=0&p=a&u=0000000001&name=%E7%94%A8%E6%88%B7001&phone=18900000001&m=6450a888a812cb92ce685a2aaf84374f";
    WebViewController *webVC = [[WebViewController alloc] init];
    webVC.path = url;
    [self.navigationController pushViewController:webVC animated:YES];
}

//车辆报价 -- HTML页面
- (void)carQuote {
    NSString *url = @"http://m.baoxianduoduo.com/QybApp/Channel/QuoteFromChannel?c=951999MB&t=0&p=a&u=0000000001&name=%E7%94%A8%E6%88%B7001&phone=18900000001&m=6450a888a812cb92ce685a2aaf84374f";
    WebViewController *webVC = [[WebViewController alloc] init];
    webVC.path = url;
    [self.navigationController pushViewController:webVC animated:YES];
}

//订单详情 -- HTML页面
- (void)orderDetail {
    NSString *url = @"http://m.baoxianduoduo.com/QybApp/Channel/OrderDetailFromChannel?c=951999MB&t=0&p=a&u=0000000001&name=%E7%94%A8%E6%88%B7001&phone=18900000001&m=6450a888a812cb92ce685a2aaf84374f";
    WebViewController *webVC = [[WebViewController alloc] init];
    webVC.path = url;
    [self.navigationController pushViewController:webVC animated:YES];
}

//渠道号
//951999MB

//安全码:
//2A769D257B1A4D60AC7502C2DE808C6E

//用户ID:
//0000000001

//订单列表 -- JSON数据
- (void)orderList {
    NSString *url = @"http://test.baoxianduoduo.com/QybApi/v2/Channel/Order/UserOrders";
    NSDictionary *dic = @{
                          @"data":@{
                                  @"FullTextSearch":@"王维一",
                                  @"InsuranceOrderStatu":@"2"
                                  },
                          @"paging":@{
                                  @"pageNumber":@"1",
                                  @"pageSize":@"5"
                                  },
                          @"userCookie":@{
                                  @"channelNumber":@"951999MB",
                                  @"UserID":@"0000000001",
                                  @"m":@"7b16f939c2ec119ec1a73784de0d69b3"
                                  }
                          };
    AFHTTPRequestOperationManager *manager = [AFHTTPRequestOperationManager manager];
    manager.requestSerializer = [AFJSONRequestSerializer serializer];
    manager.responseSerializer = [AFJSONResponseSerializer serializer];
    [manager POST:url parameters:dic success:^(AFHTTPRequestOperation *operation, id responseObject) {
        UITableView *tableView = (UITableView *)[self.view viewWithTag:101];
        [tableView removeFromSuperview];
        _isAppear = NO;
        NSString *content = [NSString stringWithFormat:@"%@", responseObject];
        self.textView.text = content;
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        NSLog(@"请求失败");
    }];
}


@end
